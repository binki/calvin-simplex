/*
 * 2010 Nathan Phillip Brink <binki@ohnopub.net>
 */

/**
 * \file
 *   A simple assembler for the simplex assembly language.
 */

#include <list.h>
#include <locale.h>
#include <stack.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "calvin-simplex-opcodes.h"
extern FILE *yyin;

struct simplex_as_input_file
{
  FILE *file;
  /* This will only point to static strings or other strings we
   * needn't free.  Thus: Don't call free() on name ;-).
   */
  char *name;
  /* 1 for normal files, 0 for stdin */
  short needs_closing;
};

void simplex_as_input_file_free(struct simplex_as_input_file *input_file);

/* globals -- published */
uint8_t op;
uint8_t addr;

FILE *output_file;
size_t in_file_line;

short output_ascii;

/* globals -- unpublished */
/**
 * \brief
 *   The current file being parsed.
 */
static char *in_file_name;
/**
 * \brief
 *   Contains type struct simplex_as_input_file *.
 */
static list_t input_files;
/**
 * \brief
 *   Whether or not we're in an ``error'' state and should stop
 *   outputting compiled code.
 */
static short in_error;
/**
 * \brief
 *   The characters that lex(1) has pushed back using unput().
 */
static lstack_t unput_chars;

int main(int argc, char *argv[])
{
  int myopt;

  short ret;

  char *output_file_name;

  struct simplex_as_input_file *input_file;

  output_file_name = NULL;
  output_file = NULL;
  output_ascii = 0;
  ret = 0;
  unput_chars = NULL;
  input_files = NULL;

  /* so that toupper() and friends work reliably: */
  setlocale(LC_CTYPE, "POSIX");

  /* ask getopt() to be verbose */
  if (!opterr)
    opterr = 1;

  while ( (myopt = getopt(argc, argv, "hm:o:V")) != -1 )
    switch (myopt)
      {
      default:
	ret = 1;
      case 'h':
	printf("Usage: %s [-h] [-m <target>] [-o <output>] [<input>] ...\n"
	       "\n"
	       " -h Show this help and exit.\n"
	       " -m <arch> Set the output architecture: one of\n"
	       "\tsimplex-ascii or simplex-bytecode. Default is\n"
	       "\tsimplex-bytecode.\n"
	       " -o <file> Set the output file. The default output\n"
	       "\tlocation is stdout.\n"
	       " -V Display the version information and exit.\n",
	       argv[0]);
	return ret;

      case 'm':
	if (!strcmp(optarg, "simplex-ascii"))
	  output_ascii = 1;
	else if (!strcmp(optarg, "simplex-bytecode"))
	  output_ascii = 0;
	else
	  {
	    fprintf(stderr, "error: -m: unrecognized option: ``%s''. Valid options\
 include ``simplex-ascii'' and ``simplex-bytecode''\n",
		    optarg);
	    return 1;
	  }
	break;

      case 'o':
	if (output_file_name)
	  {
	    fprintf(stderr, "warning: -o: This option was specified multiple times.\
 Only the last option will be used.\n");
	    free(output_file_name);
	  }
	output_file_name = strdup(optarg);
	break; 

      case 'V':
	return calvin_simplex_version();
      }

  input_files = list_init();
  /* process input files */
  for (; optind < argc; optind ++)
    {
      input_file = malloc(sizeof(struct simplex_as_input_file));
      input_file->file = fopen(argv[optind], "r");
      if (!input_file->file)
	{
	  fprintf(stderr, "error: Unable to open ``%s''\n",
		  argv[optind]);
	  free(input_file);
	  ret = 1;
	  continue;
	}
      input_file->name = argv[optind];
      input_file->needs_closing = 1;
      list_insert_after(input_files, input_file, 0);
    }
  if (ret)
    goto cleanup;

  /* default to stdin */
  if (list_empty(input_files))
    {
      input_file = malloc(sizeof(struct simplex_as_input_file));
      input_file->file = stdin;
      input_file->name = "stdin";
      input_file->needs_closing = 0;
      list_insert_after(input_files, input_file, 0);
    }

  if (output_file_name)
    {
      output_file = fopen(output_file_name, "w");
      if (!output_file)
	{
	  fprintf(stderr, "error: Unable to open output file ``%s''\n",
		  output_file_name);
	  ret = 1;
	  goto cleanup;
	}
    }
  else
    output_file = stdout;

  list_mvfront(input_files);
  if (yywrap())
    {
      fprintf(stderr, "error: yywrap() returned 1 when it shouldn't've... impossible case!\n");
      goto cleanup;
    }
  unput_chars = stack_init();
  yyparse();

 cleanup:
  if (unput_chars)
    stack_free(unput_chars, STACK_DEALLOC);
  if (input_files)
    list_free(input_files, (list_dealloc_func_t)&simplex_as_input_file_free);
  free(output_file_name);

  return ret;
}

void simplex_as_input_file_free(struct simplex_as_input_file *input_file)
{
  if (!input_file)
    return;

  if (input_file->needs_closing)
    fclose(input_file->file);

  /*
   * ->name needn't be free()d because we don't use malloc()/strdup()
   * for it.
   */

  free(input_file);
}

void simplex_as_write_opcode(FILE *out, short ascii, size_t op_index, uint8_t addr)
{
  if (in_error)
    return;

  if (ascii)
    {
      fprintf(out, "%02d%02d\n", calvin_simplex_opcodes[op_index].opcode, addr);
      return;
    }

  putc(calvin_simplex_opcodes[op_index].opcode, out);
  putc(addr, out);
}

void simplex_as_set_error(const char *msg, ...)
{
  va_list ap;

  va_start(ap, msg);
  fprintf(stderr, "%s:%d: error: ", in_file_name, in_file_line);
  vfprintf(stderr, msg, ap);
  fprintf(stderr, "\n");
  va_end(ap);

  in_error = 1;
}

/**
 * \brief
 *   Implement yywrap() to enabled concatenating multiple input files
 *   like the ``real'' as(1) does.
 */
int yywrap()
{
  struct simplex_as_input_file *input_file;

  if ( (input_file = list_remove_curr(input_files)) )
    {
      fprintf(stderr, "info: parsing ``%s''\n", input_file->name);
      in_file_line = 0;
      in_file_name = input_file->name;
      yyin = input_file->file;

      /* 0 means to continue parsing with the newly-set-up input */
      return 0;
    }

  /* 1 means to end the parsing */
  return 1;
}
