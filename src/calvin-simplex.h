/*
 * 2010 Nathan Phillip Brink <binki@ohnopub.net>
 */

#ifndef _CALVIN_SIMPLEX_H
#define _CALVIN_SIMPLEX_H

#include <stdint.h>

struct calvin_simplex_machine_state
{
  /* ``InstrReg'' */
  uint64_t instr;
  /* ``ProgCntr'' */
  unsigned short pc;
  /* ``Accum'' */
  int16_t accum;
  /* ``XReg'' */
  int16_t x;

  int16_t mem[100];

  /* set to NULL if there is no error */
  char *error;
};
typedef struct calvin_simplex_machine_state *calvin_simplex_machine_state_t;

#endif
