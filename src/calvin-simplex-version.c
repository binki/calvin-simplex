/*
 * 2011 Nathan Phillip Brink <ohnobinki@ohnopublishing.net>
 */

#include <stdio.h>

int calvin_simplex_version()
{
  printf(PACKAGE_STRING "\n"
	 "2011 Nathan Phillip Brink <ohnobinki@ohnopublishing.net>\n"
	 "\n"
	 "Provided as-is, without any warranty to the extent permitted by law.\n"
	 "Provided for convenience without intention to deprive glory from the\n"
	 "unknown, undocumented person who invented the Simplex machine.\n");
  return 0;
}
