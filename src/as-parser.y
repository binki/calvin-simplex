%{
#include <stdint.h>
#include <stdio.h>
#include "simplex-vm-unknown-calvin-as.h"

%}

%token NEWLINE WHITESPACE

%token ADDRESS
%token OP OP_ARG OP_NONEXISTENT

%start program
%%
program : NEWLINE program
	| line program
	| line
        | NEWLINE 
        | ;

line :
instruction
        | WHITESPACE instruction NEWLINE
        | instruction NEWLINE
	;

instruction :
	op_arg WHITESPACE address
	{ simplex_as_write_opcode(output_file, output_ascii, $1, $3); }
	| op
	{ simplex_as_write_opcode(output_file, output_ascii, $1, 0); }
	;

op_arg : OP_ARG
{ $$ = op; }
;
op : OP
{ $$ = op; }
;

address :
ADDRESS
{ $$ = addr; if (addr > 99) { simplex_as_set_error("The given address, %d, is greater than 99\n", addr); } }
;
