/*
 * 2010 Nathan Phillip Brink <binki@ohnopub.net>
 */

#ifndef _SIMPLEX_VM_UNKNOWN_CALVIN_AS_H
#define _SIMPLEX_VM_UNKNOWN_CALVIN_AS_H

/**
 * \file
 *   Declarations that the lex and yacc files need to know about.
 */

#include <stdint.h>
#include <stdlib.h>

/**
 * \brief
 *   The last parsed operation.
 *
 * To be set by the OP/OP_ARG rule of as-lexer.l and accessed by the
 * op or op_arg rules of as-lexer.y.
 */
extern uint8_t op;

/**
 * \brief
 *   The last parsed address.
 *
 * To be set by the ADDRESS rule of as-lexer.l and accessed by the
 * op_arg rule of as-parser.y.
 */
extern uint8_t addr;

/**
 * \brief
 *   The current line number of the file being read.
 *
 * To be incremented by the input() function when appropriate.
 */
extern size_t in_file_line;

/**
 * \brief
 *   The file being written to.
 *
 * To be used by the output rules in as-parser.y
 */
extern FILE *output_file;

/**
 * \brief
 *   Indicate that ASCII output is desired instead of bytecode.
 */
extern short output_ascii;

/**
 * \brief
 *   Inform the user of an error and set the error state.
 *
 * \param msg
 *   The message, with printf() -style escapes.
 */
extern void simplex_as_set_error(const char *msg, ...);

/**
 * \brief
 *   Writes out an opcode.
 *
 * \param out
 *   stream to write the opcodes to
 * \param ascii
 *   Whether or not to write output suitable for calvin-simplex(1)'s -a option.
 * \param op_index
 *   The op's index in the calvin_simplex_opcodes array.
 * \param addr
 *   The address given with the operation or 0 if the operation takes no address.
 */
extern void simplex_as_write_opcode(FILE *out, short ascii, size_t op_index, uint8_t addr);

/**
 * \brief
 *   Override lex(1)'s input() to allow reading from the
 *   user-specified input files
 *//*
     extern int as_input();*/

/**
 * \brief
 *   Override lex(1)'s output() to avoid allowing lex to write to stdout.
 *//*
     extern void as_output(int d);*/

/**
 * \brief
 *   Override lex(1)'s unput() to cooperated with input().
 *//*
     extern void as_unput(int d);*/

#endif /* _SIMPLEX_VM_UNKNOWN_CALVIN_AS_H */
