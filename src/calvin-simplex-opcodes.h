/*
 * 2010 Nathan Phillip Brink <binki@ohnopub.net>
 *
 * Opcodes are derived from Calvin College ENGR-220
 */

#ifndef _CALVIN_SIMPLEX_OPCODES_H
#define _CALVIN_SIMPLEX_OPCODES_H

#include "calvin-simplex.h"

/**
 * \brief
 *   A function which implements the functionality of an opcode.
 *
 * \param state
 *   The global state of the virtual simplex machine.
 * \param arg
 *   The 4-bit argument that each opcode gets.
 * \return
 *   0 to continue operation, 1 to halt the machine after checking for an error state.
 */
typedef int(*calvin_simplex_opcode_func_t)(calvin_simplex_machine_state_t state, uint8_t arg);

struct calvin_simplex_opcode
{
  uint8_t opcode;
  const char *name;
  calvin_simplex_opcode_func_t func;
  const char *desc;
  /* number of args: valid values: 0, 1 */
  unsigned short args;
};

extern struct calvin_simplex_opcode calvin_simplex_opcodes[];

#endif

