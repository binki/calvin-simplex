/*
 * 2010 Nathan Phillip Brink <binki@ohnopub.net>
 */

#include "calvin-simplex.h"
#include "calvin-simplex-opcodes.h"
#include "calvin-simplex-version.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void *byte_state_init();
uint16_t byte_parse_opcode(FILE *in, void *state);
void byte_state_free(void *state);

void *ascii_state_init();
uint16_t ascii_parse_opcode(FILE *in, void *state);
void ascii_state_free(void *state);

int main(int argc, char *argv[])
{
  short ascii_mode;
  short verbose;

  int ret;
  uint8_t counter;
  int arg;
  uint16_t(*parse_opcode)(FILE* in, void *state);
  void *parse_state;

  struct calvin_simplex_opcode ops[100];

  struct calvin_simplex_machine_state state;

  char *input_file_name;
  FILE *input_file;

  if (!opterr)
    opterr = 1;

  ascii_mode = 0;
  verbose = 0;
  ret = 0;

  while ( (arg = getopt(argc, argv, "avV")) != -1)
    switch (arg)
      {
      case 'a':
	ascii_mode = 1;
	fprintf(stderr, "info: enabling ASCII mode.\n");
	break;
      case 'v':
	verbose = 1;
	fprintf(stderr, "info: enabling verbose mode.\n");
	break;

      case 'V':
	return calvin_simplex_version();

      default:
	/* covers the '?' case too */
	ret = 1;
      case 'h':
	fprintf(stderr, "Usage: %s [-h] [-V] [-a] [-v] <input>\n"
		"\n"
		" -a Enable ASCII mode.\n"
		" -h Show this help.\n"
		" -v Enable verbose mode.\n"
		" -V Display the version info and exit.\n",
		argv[0]);
	return ret;
      }

  if (optind >= argc)
    {
      fprintf(stderr, "error: The last argument needs to be the input file to run.\n");
      return 1;
    }
    
  input_file_name = argv[optind];
  optind ++;

  input_file = fopen(input_file_name, "r");
  if (!input_file)
    {
      fprintf(stderr, "error: Unable to open file %s for input.\n", input_file_name);
      return 1;
    }

  if (ascii_mode)
    {
      parse_state = ascii_state_init();
      parse_opcode = &ascii_parse_opcode;
    }
  else
    {
      parse_state = byte_state_init();
      parse_opcode = &byte_parse_opcode;
    }

  /* initialize the machine */

  state.error = NULL;
  state.pc = 0;
  state.instr = 0;
  state.accum = 0;
  state.x = 0;
  memset(state.mem, 0, sizeof(state.mem));

  /* prefill memory with code */
  for (state.pc = 0;
       state.pc < 100;
       state.pc ++)
    {
      state.mem[state.pc] = (*parse_opcode)(input_file, parse_state);
    }
  state.pc = 0;
  fclose(input_file);

  /* fill the ops var */
  if (verbose)
    fprintf(stderr, "info: hashing opcodes...\n");
  counter = 0;
  for (arg = 0; arg < 100; arg ++)
    {
      /* default to there being no func here */
      ops[arg].func = NULL;
      while (calvin_simplex_opcodes[counter].func
	     && calvin_simplex_opcodes[counter].opcode < arg)
	counter ++;
      if (calvin_simplex_opcodes[counter].opcode == arg)
	{
	  memcpy(&ops[arg], &calvin_simplex_opcodes[counter], sizeof(struct calvin_simplex_opcode));
	}
    }
  if (verbose)
    fprintf(stderr, "info: done hashing opcodes done\n");

  /* act as the virtual machine */
  while (1)
    {
      if (state.pc >= 99)
	{
	  state.error = "Segmentation Fault: Attempt to continue execution outside of memory range.";
	  break;
	}

      /* fetch the new instruction and increment pc */
      state.instr = state.mem[state.pc ++];

      /* decode instruction */
      counter = (state.instr & 0xff00) >> 8;
      /* check for illegal isntruction */
      if (counter > 100
	  || !ops[counter].func)
	{
	  state.error = "Illegal instruction encountered";
	  break;
	}
      if (verbose)
	fprintf(stderr, "%s %d\n", ops[counter].name, (int)state.instr & 0x00ff);

      /* if the function returns nonzero, it's an error or we're halting */
      if ((*ops[counter].func)(&state, state.instr & 0x00ff))
	break;
    }
  if (state.error)
    {
      fprintf(stderr, "error: %s\n", state.error);
      fprintf(stderr, "error: pc=%d, instr=%d,%d, x=%d, accum=%d\n",
	      (int)state.pc, (int)(state.instr & 0xff00) >> 8,
	      (int)state.instr & 0x00ff, (int)state.x, (int)state.accum);
    }

  if (verbose)
    fprintf(stderr, "info: execution ended\n");
  
  if (ascii_mode)
    ascii_state_free(parse_state);
  else
    byte_state_free(parse_state);

  return 0;
}

void *ascii_state_init()
{
  return NULL;
}

void *byte_state_init()
{
  return NULL;
}

/**
 * \brief
 *   Parse an opcode encoded in ASCII.
 *
 * \param in
 *   The stream from where to grab these ASCII chars.
 * \param state
 *   My personal set of data (unused for this particular implementation).
 * \return
 *   The parsede opcode ready to be stored into memory.
 */
uint16_t ascii_parse_opcode(FILE *in, void *state)
{
  char instrbuf[3];
  char argbuf[3];

  if (fread(instrbuf, 2, 1, in)
      && fread(argbuf, 2, 1, in))
    {
      /*
       * support newlines after each instruction (but not windows
       * ones... IT'S JUST NOT WORTH IT!)
       */
      if (instrbuf[0] == '\n')
	{
	  instrbuf[0] = instrbuf[1];
	  instrbuf[1] = argbuf[0];
	  argbuf[0] = argbuf[1];
	  if (!fread(&argbuf[1], 1, 1, in))
	    {
	      return 0;
	    }
	}

      /* NULLs */
      instrbuf[2] = '\0';
      argbuf[2] = '\0';

      /*
       * pack the instruction in the higher four bits and the argument
       * into the lower 4 bits. This seems to conform best to the
       * specs...
       */
      return (atoi(instrbuf) & 0x00ff) << 8
	| (atoi(argbuf) & 0x00ff);
    }

  return 0;
}

uint16_t byte_parse_opcode(FILE *in, void *state)
{
  uint8_t op;
  uint8_t addr;

  if (!fread(&op, sizeof(op), 1, in)
      || !fread(&addr, sizeof(op), 1, in))
    {
      return 0;
    }

  return op << 8 | addr;
}

void ascii_state_free(void *state)
{
}

void byte_state_free(void *state)
{
}
