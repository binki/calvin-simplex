/*
 * 2010 Nathan Phillip Brink <binki@ohnopub.net>
 *
 * opcodes copied from ENGR-220 class taught by Professor Steven
 * VanderLeest <svleest@calvin.edu>
 */

#include "calvin-simplex-opcodes.h"
#include "calvin-simplex.h"

#include <stdio.h>

static int store_value(calvin_simplex_machine_state_t state, uint8_t loc, int16_t val);

/* halt */
static int op_halt(calvin_simplex_machine_state_t state, uint8_t arg);

/* I/O */
static int op_read(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_write(calvin_simplex_machine_state_t state, uint8_t arg);

/* accumulator register */
static int op_cleara(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_loada(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_storea(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_add(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_subtract(calvin_simplex_machine_state_t state, uint8_t arg);

/* control */
static int op_jump(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_jumpaz(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_jumpan(calvin_simplex_machine_state_t state, uint8_t arg);

/* X register */
static int op_clearx(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_loadx(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_storex(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_incx(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_decx(calvin_simplex_machine_state_t state, uint8_t arg);
static int op_jumpx(calvin_simplex_machine_state_t state, uint8_t arg);

/*
 * In the following, ``mm'' refers to the argument to the opcode.
 */

struct calvin_simplex_opcode calvin_simplex_opcodes[] =
  {
    /* halt */
    {
      .opcode = 00,
      .name = "HALT",
      .func = &op_halt,
      .desc = "Stop execution (useful for indicating the end of the program).\n\
Stop execution and check the result.",
      .args = 0,
    },

    /* I/O */
    {
      .opcode = 10,
      .name = "READ",
      .func = &op_read,
      .desc = "Read a word of data from the keyboard and store in memory location mm.\n\
1. Ask input for a number.\n\
2. Tell memory to put that number into memory location mm.",
      .args = 1,
    },
    {
      .opcode = 11,
      .name = "WRITE",
      .func = &op_write,
      .desc = "Write a word from memory location mm to the monitor.\n\
1. Ask memory for the number in location mm.\n\
2. Tell output to display that number.",
      .args = 1,
    },

    /* special accumulator register things */
    {
      .opcode = 20,
      .name = "CLEARA",
      .func = &op_cleara,
      .desc = "Clear the accumulator so its value is 0000.\n\
1. Tell Accumulator to reset to 0000.",
      .args = 0,
    },
    {
      .opcode = 21,
      .name = "LOADA",
      .func = &op_loada,
      .desc = "Load a value from memory location mm into the accumulator.\n\
1. Ask memory for the number in location mm.\n\
2. Give the number to the accumulator.",
      .args = 1,
    },
    {
      .opcode = 22,
      .name = "STOREA",
      .func = &op_storea,
      .desc = "Store the accumulator's data into memory location mm.\n\
1. Ask accumulator what it has.\n\
2. Tell memory to store that number in location mm.",
      .args = 1,
    },
    {
      .opcode = 23,
      .name = "ADD",
      .func = &op_add,
      .desc = "Add the data in memory location mm to the accumulator,\
 leaving the sum in the accumulator.\n\
1. Ask memory for the number in location mm.\n\
2. Tell accumulator to add that number to whatever it had before.",
      .args = 1,
    },
    {
      .opcode = 24,
      .name = "SUBTRACT",
      .func = &op_subtract,
      .desc = "Subtract the data in mem location mm from the accumulator, leaving the difference\
 there.\n\
1. Ask memory for the number in location mm.\n\
2. Tell accumulator to subtract that number from whatever it had before.",
      .args = 1,
    },

    /* control flow */
    {
      .opcode = 30,
      .name = "JUMP",
      .func = &op_jump,
      .desc = "Branch to location mm in memory by setting the ProgCntr to mm.\n\
1. Tell Program Counter to change to the value mm.",
      .args = 1,
    },
    {
      .opcode = 31,
      .name = "JUMPAZ",
      .func = &op_jumpaz,
      .desc = "Branch to location mm in memory if the accumulator currently holds 0000.\n\
1. Ask accumulator if it is 0000.\n\
2. If \"YES\", tell Program Counter to change to mm; else do nothing.",
      .args = 1,
    },
    {
      .opcode = 32,
      .name = "JUMPAN",
      .func = &op_jumpan,
      .desc = "Branch to location mm in memory if the accumulator currently holds a negative\
 number.\n\
1. Ask accumulator if it is negative.\n\
2. If \"YES\", tell Program Counter to change to mm; else do nothing.",
      .args = 1,
    },

    /* The X register */
    {
      .opcode = 40,
      .name = "CLEARX",
      .func = &op_clearx,
      .desc = "Clear the X register (set it equal to 0000)\n\
1. Tell the X register to set itself to 0000.",
      .args = 0,
    },
    {
      .opcode = 41,
      .name = "LOADX",
      .func = &op_loadx,
      .desc = "Load the contents of memory location mm into the X register.\n\
1. Ask memory for the number in location mm.\n\
2. Give the number to the X register.",
      .args = 1,
    },
    {
      .opcode = 42,
      .name = "STOREX",
      .func = &op_storex,
      .desc = "Store the contents of the X register into memory location mm.\n\
1. Ask the X register for the number it has.\n\
2. Tell memory to store the number in location mm.",
      .args = 1,
    },
    {
      .opcode = 43,
      .name = "INCX",
      .func = &op_incx,
      .desc = "Increment the X register (add 1 to the current value).\n\
1. Tell the X register to add one to itself.",
      .args = 0,
    },
    {
      .opcode = 44,
      .name = "DECX",
      .func = &op_decx,
      .desc = "Decrement the X register (subtract 1 from the current value).\n\
1. Tell the X register to subtract one from itself.",
      .args = 0,
    },
    {
      .opcode = 45,
      .name = "JUMPX",
      .func = &op_jumpx,
      .desc = "Branch to location mm in memory if the X register currently holds 0000.\n\
1. Ask the X register if it currently has 0000.\n\
2. If \"YES\", tell Program Counter to change to mm; else do nothing.",
      .args = 1,
    },
    {
      .opcode = 0,
      .name = NULL,
      .func = NULL,
      .desc = NULL,
      .args = 0,
    },
  };

static int store_value(calvin_simplex_machine_state_t state, uint8_t loc, int16_t val)
{
  if (loc >= sizeof(state->mem))
    {
      state->error = "Segmentation Fault: Attempt to store data in out of range memory location.";
      return 1;
    }

  state->mem[loc] = val;

  return 0;
}

static int retrieve_value(calvin_simplex_machine_state_t state, uint8_t loc, int16_t *val)
{
  if (loc >= sizeof(state->mem))
    {
      state->error = "Segmentation Fault: Attempt to retrieve data from out of range memory location.";
      return 1;
    }

  *val = state->mem[loc];

  return 0;
}

/* halt */
static int op_halt(calvin_simplex_machine_state_t state, uint8_t arg)
{
  return 1;
}

/* I/O */
static int op_read(calvin_simplex_machine_state_t state, uint8_t arg)
{
  int in;

  printf(" > ");
  scanf("%d", &in);

  return store_value(state, arg, (int16_t)in);
}

static int op_write(calvin_simplex_machine_state_t state, uint8_t arg)
{
  int16_t val;
  int ret;

  ret = retrieve_value(state, arg, &val);
  if (ret)
    return ret;

  printf("[%02d] = %04d\n", arg, (int)val);

  return ret;
}

/* accumulator register */
static int op_cleara(calvin_simplex_machine_state_t state, uint8_t arg)
{
  state->accum = 0;

  return 0;
}

static int op_loada(calvin_simplex_machine_state_t state, uint8_t arg)
{
  int16_t val;
  int ret;

  ret = retrieve_value(state, arg, &val);
  if (ret)
    return ret;

  state->accum = val;

  return 0;
}

static int op_storea(calvin_simplex_machine_state_t state, uint8_t arg)
{
  return store_value(state, arg, state->accum);
}

static int op_add(calvin_simplex_machine_state_t state, uint8_t arg)
{
  int16_t val;
  int ret;

  ret = retrieve_value(state, arg, &val);
  if (ret)
    return ret;

  state->accum += val;

  return 0;
}

static int op_subtract(calvin_simplex_machine_state_t state, uint8_t arg)
{
  int16_t val;

  if (retrieve_value(state, arg, &val))
    return 1;

  state->accum -= val;

  return 0;
}


/* control */
static int op_jump(calvin_simplex_machine_state_t state, uint8_t arg)
{
  if (arg > 99)
    {
      state->error = "Segmentation Fault: Attempt to jump outside of memory range.";
      return 1;
    }

  state->pc = arg;

  return 0;
}

static int op_jumpaz(calvin_simplex_machine_state_t state, uint8_t arg)
{
  if (state->accum == 0)
    return op_jump(state, arg);

  return 0;
}

static int op_jumpan(calvin_simplex_machine_state_t state, uint8_t arg)
{
  if (state->accum < 0)
    return op_jump(state, arg);

  return 0;
}


/* X register */
static int op_clearx(calvin_simplex_machine_state_t state, uint8_t arg)
{
  state->x = 0;

  return 0;
}

static int op_storex(calvin_simplex_machine_state_t state, uint8_t arg)
{
  return store_value(state, arg, state->x);
}

static int op_loadx(calvin_simplex_machine_state_t state, uint8_t arg)
{
  int16_t val;

  if (retrieve_value(state, arg, &val))
    return 1;

  state->x = val;

  return 0;
}

static int op_incx(calvin_simplex_machine_state_t state, uint8_t arg)
{
  state->x ++;

  return 0;
}

static int op_decx(calvin_simplex_machine_state_t state, uint8_t arg)
{
  state->x --;

  return 0;
}

static int op_jumpx(calvin_simplex_machine_state_t state, uint8_t arg)
{
  if (state->x == 0)
    return op_jump(state, arg);

  return 0;
}
