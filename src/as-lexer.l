%{

#include "as-parser.h"
#include <ctype.h>

#include "simplex-vm-unknown-calvin-as.h"

int yywrap();


void yyerror(char *error);
static int as_lexer_op_find_token(char *op);
%}

%%
[ \t\r]*(;[^\n]*)?\n { /* fprintf(stderr, "newline\n"); */ in_file_line ++; return NEWLINE; }
[ \t\r]+ { /* fprintf(stderr, "whitespace\n"); */ return WHITESPACE; }


[a-z]+ { /* fprintf(stderr, "op\n"); */ return as_lexer_op_find_token(yytext); };
[0-9]+ { addr = atoi(yytext); /* fprintf(stderr, "found address: %d\n", addr); */ return ADDRESS; };

. { /* fprintf(stderr, "found bad stuffage\n"); */ return OP_NONEXISTENT; };
%%

#include "calvin-simplex-opcodes.h"

static int as_lexer_op_find_token(char *op_name)
{
  size_t counter;
  char op_name_upper[10];

  for (counter = 0;
       op_name[counter]
	 && counter < sizeof(op_name_upper);
       counter ++)
    op_name_upper[counter] = toupper(op_name[counter]);
  if (counter < sizeof(op_name_upper))
    op_name_upper[counter] = '\0';

  for (counter = 0; calvin_simplex_opcodes[counter].name; counter ++)
    {
      if (!strncmp(op_name_upper, calvin_simplex_opcodes[counter].name, sizeof(op_name_upper)))
	{
	  op = counter;
	  /* fprintf(stderr, "found an op %s with %d args\n", calvin_simplex_opcodes[counter].name, calvin_simplex_opcodes[counter].args); */
	  if (calvin_simplex_opcodes[counter].args)
	    return OP_ARG;
	  return OP;
	}
    }

  /* fprintf(stderr, "found a nonexistent thing: %s\n", op_name_upper); */
  simplex_as_set_error("No such op: %s", op_name);
  return OP_NONEXISTENT;
}

void yyerror(char *error)
{
  simplex_as_set_error("%s", error);
}
