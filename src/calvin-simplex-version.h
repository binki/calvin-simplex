/*
 * 2011 Nathan Phillip Brink <ohnobinki@ohnopublishing.net>
 */

/**
 * \brief
 *   Implement the -V option, which prints out the calvin-simplex
 *   version.
 */
int calvin_simplex_version();
