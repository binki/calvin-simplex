	;; 2010 Nathan Phillip Brink <binki@ohnopub.net>
	;;
	;; An multiplier written for calvin-simplex.
	;; http://ohnopub.net/hg/calvin-simplex
	;; 
	
	;; initialize registers (0)
	cleara
	clearx

	;; eat operands (2)
	;;
	;; We're going to use the operand in 50 with our accumulator and
	;; the operand in 51 with our X register.
	read 50
	read 51

	;; check for negativity (4)
	loada 50
	jumpan 9
	;; if the accumulator is negative, multiply as normal (6)
	loada 51
	jumpan 18
	jump 18

	;; check for double negativity (9)
	loada 51
	jumpan 12
	jump 18

	;; un-negate the two negative numbers (12)
	cleara
	subtract 51
	storea 51
	
	cleara
	subtract 50
	storea 50

	;; multiply two positive numbers (18)
	cleara
	loadx 51
	;; loop begins, check whether or not we're done (20)
	jumpx 32
	add 50
	decx
	jump 20

	;; multiply a positive (accumulator) and negative (X register) number (24)
	loada 50
	loadx 51
	;; If the accumulator is the only thing negative,
	;; the positive multiplication should work (26)
	jumpan 18
	cleara
	;; loop begins for a negative x register and positive accumulator (28)
	jumpx 32
	subtract 50
	incx
	jump 28
	
	;; output result (32)
	storea 52
	write 52
	
	;; done (34)
	halt
